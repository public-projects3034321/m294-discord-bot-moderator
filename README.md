# M294 Discord Bot Moderator

This project has been made in accordance to a project exercise in the module M294. It has been made
in the second year of my education, during the first semester of it. 

Like the name suggests, this project implements a Discord moderation bot, along with an API, as well as an user interface to manually control certain aspects.